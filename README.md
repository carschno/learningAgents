learningAgents
==============
A framework for a simulation in which free agents learn common "languages" from each other by reacting on random external signals.
